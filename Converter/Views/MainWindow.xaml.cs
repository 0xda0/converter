﻿using MahApps.Metro.Controls;
using Converter.ViewModel.Interfaces;

namespace Converter.Views
{
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            IClose context = DataContext as IClose;
            context.Closing();
        }
    }
}
