﻿namespace Converter.Data
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using Converter.Model;

    public class PersonContext : DbContext
    {
        public PersonContext()
            : base("name=PersonContext")
        {
        }

        public DbSet<Person> Person { get; set; }
    }
}