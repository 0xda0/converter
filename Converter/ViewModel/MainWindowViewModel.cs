﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Converter.ViewModel.Commands;
using Microsoft.Win32;
using Converter.Data;
using Converter.Model;
using System.IO;
using System.Xml.Serialization;
using System.Data.Entity;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using System.Diagnostics;
using Converter.ViewModel.Interfaces;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace Converter.ViewModel
{
    public class MainWindowViewModel : BaseWindowViewModel, IClose
    {
        [DllImport("User32.dll")]
        public static extern int GetWindowThreadProcessId(IntPtr hWnd, out int processId);
        public static ObservableCollection<Person> TableInfo { get; set; } = new ObservableCollection<Person>();
        public static PersonContext Db = new PersonContext();
        public static IQueryable<Person> Pers = Db.Person;
        private bool busy;
        private BaseWindowViewModel ViewModel { get; set; }
        public ICommand UpdateViewCommand { get; set; }

        public bool IsBusy
        {
            get { return busy; }
            set
            {
                busy = value;
                OnPropertyChanged("IsBusy");
            }
        }
        public BaseWindowViewModel SelectedViewModel
        {
            get => ViewModel;
            set
            {
                ViewModel = value;
                OnPropertyChanged("SelectedViewModel");
            }
        }


        public MainWindowViewModel()
        {
            SelectedViewModel = new DataViewModel();
            UpdateViewCommand = new UpdateView(this);
        }


        public ICommand AddFile
        {
            get
            {
                return new Command(() =>
                {
                    var file = new OpenFileDialog()
                    {
                        Filter = ".csv file |*.csv",
                        DefaultExt = ".csv"
                    };

                    if (file.ShowDialog() == true)
                    {
                        ReadCsvFile(file.FileName);
                    }
                });
            }
        }

        public ICommand ExportExcel
        {
            get
            {
                return new Command(() =>
                {
                    var saveExcel = new SaveFileDialog()
                    {
                        Filter = ".xlsx file|*.xlsx",
                        DefaultExt = ".xlsx"
                    };

                    if (saveExcel.ShowDialog() == true)
                    {
                        ExportToExcel(saveExcel.FileName);
                    }
                });
            }
        }


        public ICommand ExportXml
        {
            get
            {
                return new Command(() =>
                {
                    var saveXml = new SaveFileDialog()
                    {
                        Filter = ".xml file|*.xml",
                        DefaultExt = ".xml"
                    };
                    if (saveXml.ShowDialog() == true)
                    {
                        ExportToXml(saveXml.FileName);
                    }
                });
            }
        }

        private async void ReadCsvFile(string path)
        {
            try
            {
                List<Person> data = new List<Person>();
                await Task.Run(() =>
                {
                    IsBusy = true;
                    using (var streamRead = File.OpenText(path))
                    {
                        while (!streamRead.EndOfStream)
                        {
                            string line = streamRead.ReadLine();
                            string[] split = line.Split(new[] { ';' });
                            Person p = (new Person()
                            {
                                Date = DateTime.ParseExact(split[0], "dd/MM/yyyy", null),
                                FirstName = split[1],
                                LastName = split[2],
                                SurName = split[3],
                                City = split[4],
                                Country = split[5]
                            });
                            data.Add(p);
                        }
                    }
                    Bulk.AddToTable(data);
                    App.Current.Dispatcher.Invoke((Action)delegate
                    {
                        data.ToList().ForEach(TableInfo.Add);
                    });
                    IsBusy = false;
                });
            }
            catch (Exception str)
            {
                MessageBox.Show(str.Message);
            }
        }

        private async void ExportToExcel(string path)
        {
            await Task.Run(() =>
            {
                IsBusy = true;
                var excel = new Excel.Application();
                try
                {
                    var wb = excel.Workbooks.Add();
                    Excel.Worksheet ws = (Excel.Worksheet)excel.ActiveSheet;
                    ws.Cells[1, "A"] = "Дата";
                    ws.Cells[1, "B"] = "Имя";
                    ws.Cells[1, "C"] = "Фамилия";
                    ws.Cells[1, "D"] = "Отчество";
                    ws.Cells[1, "E"] = "Город";
                    ws.Cells[1, "F"] = "Страна";

                    var dataList = Pers.AsNoTracking().ToList();
                    int columns = dataList.Count;
                    const int rows = 6;

                    object[,] data = new object[columns, rows];
                    for (int i = 0; i < columns; i++)
                    {
                        Person p = dataList[i];
                        data[i, 0] = p.Date;
                        data[i, 1] = p.FirstName;
                        data[i, 2] = p.LastName;
                        data[i, 3] = p.SurName;
                        data[i, 4] = p.City;
                        data[i, 5] = p.Country;
                    }

                    Excel.Range startCell = (Excel.Range)ws.Cells[2, 1];
                    Excel.Range endCell = (Excel.Range)ws.Cells[columns, rows];
                    Excel.Range range = ws.get_Range(startCell, endCell);
                    range.Value = data;
                    wb.SaveAs(path);
                    IsBusy = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Excel: " + ex.Message);
                }
                finally
                {
                    ExcelKill(excel);
                }
            });
        }

        private void ExcelKill(Excel.Application excelApp)
        {
            if (excelApp != null)
            {
                int excelId = 0;
                GetWindowThreadProcessId(new IntPtr(excelApp.Hwnd), out excelId);
                Process excelProc = Process.GetProcessById(excelId);
                if (excelProc != null)
                    excelProc.Kill();
            }
        }

        private async void ExportToXml(string path)
        {
            await Task.Run(() =>
            {
                IsBusy = true;
                var data = Pers.AsNoTracking().ToList();
                var serial = new XmlSerializer(typeof(List<Person>));
                using (var fs = new FileStream(path, FileMode.Create, FileAccess.Write))
                {
                    serial.Serialize(fs, data);
                }
                IsBusy = false;
            });
        }

        public void Closing()
        {
            Bulk.ClearTable();
        }
    }
}