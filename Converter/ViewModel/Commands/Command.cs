﻿using System;
using System.Collections.Generic;
using System.Windows.Input;

namespace Converter.ViewModel.Commands
{
    class Command : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private Action Click;

        public Command(Action click)
        {
            Click = click;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            Click.Invoke();
        }
    }
}
