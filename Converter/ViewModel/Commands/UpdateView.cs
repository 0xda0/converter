﻿using System;
using System.Collections.Generic;
using System.Windows.Input;

namespace Converter.ViewModel.Commands
{
    class UpdateView : ICommand
    {
        private MainWindowViewModel ViewModel;

        public UpdateView(MainWindowViewModel viewModel)
        {
            this.ViewModel = viewModel;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            if (parameter.ToString() == "Settings")
                ViewModel.SelectedViewModel = new ExportSettingsViewModel();
            if (parameter.ToString() == "Data")
                ViewModel.SelectedViewModel = new DataViewModel();
        }
    }
}
