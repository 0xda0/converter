﻿using System;
using System.Collections.ObjectModel;
using Converter.Model;

namespace Converter.ViewModel
{
    public class DataViewModel : BaseWindowViewModel
    {
        public ObservableCollection<Person> DataInfo { get; set; }
        public DataViewModel()
        {
            DataInfo = MainWindowViewModel.TableInfo;
        }
    }
}
