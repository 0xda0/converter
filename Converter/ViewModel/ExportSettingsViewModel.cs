﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Converter.ViewModel.Commands;
using System.Windows.Input;
using Microsoft.Win32;
using Converter.Model;
using System.Windows;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;

namespace Converter.ViewModel
{
    public class ExportSettingsViewModel : BaseWindowViewModel
    {
        private DateTime? _date;
        private string _firstname;
        private string _lastname;
        private string _surname;
        private string _city;
        private string _country;

        public DateTime? DateText
        {
            get =>  _date;
            set
            {
                _date = value;
                OnPropertyChanged("Date");
            }
        }

        public string FirstNameText
        {
            get => _firstname; 
            set
            {
                _firstname = value;
                OnPropertyChanged("FirstName");
            }
        } 

        public string LastNameText
        {
            get =>_lastname; 
            set
            {
                _lastname = value;
                OnPropertyChanged("LastName");
            }
        }


        public string SurNameText
        {
            get => _surname; 
            set
            {
                _surname = value;
                OnPropertyChanged("SurName");
            }
        }


        public string CityText
        {
            get => _city; 
            set
            {
                _city = value;
                OnPropertyChanged("City");
            }
        }

        public string CountryText
        {
            get => _country; 
            set
            {
                _country = value;
                OnPropertyChanged("Country");
            }
        }
        private async void Message(string title, string message)
        {
            var metroWindow = (Application.Current.MainWindow as MetroWindow);
            await metroWindow.ShowMessageAsync(title, message);
        }
        public ICommand ChangeExportData
        {
            get
            {
                return new Command(() =>
                {
                    IQueryable<Person> query = MainWindowViewModel.Db.Person;

                    if (DateText != null)
                        query = query.Where(p => p.Date == DateText);
                    if (FirstNameText != null)
                        query = query.Where(fn => fn.FirstName == FirstNameText);
                    if (LastNameText != null)
                        query = query.Where(ln => ln.LastName == LastNameText);
                    if (SurNameText != null)
                        query = query.Where(sn => sn.SurName == SurNameText);
                    if (CityText != null)
                        query = query.Where(c => c.City == CityText);
                    if (CountryText != null)
                        query = query.Where(cn => cn.Country == CountryText);


                    MainWindowViewModel.Pers = query;

                    Message("Уведомление", "Выбрано " + query.Count().ToString() + " записей.");
                });
            }
        }
        public ICommand ResetExportData
        {
            get
            {
                return new Command(() =>
                {
                    MainWindowViewModel.Pers = MainWindowViewModel.Db.Person;
                    Message("Уведомление", "Сброс завершен.");
                });
            }
        }

    }
}
