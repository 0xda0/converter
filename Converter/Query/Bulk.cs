﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Configuration;
using FastMember;
using System.Windows;
using Converter.Model;
using System.Threading.Tasks;

namespace Converter
{
    class Bulk
    {
        private static string connectionString { get; set; } = ConfigurationManager.ConnectionStrings["PersonContext"].ConnectionString;
        private const string tableName  = "People";
        private const int batchSize = 100000;
        private static SqlConnection ConnectSql()
        {
            StringBuilder errorMsg = new StringBuilder();
            SqlConnection connection = new SqlConnection(connectionString);
            try
            {
                connection.Open();
            }
            catch (SqlException ex)
            {
                for (int i = 0; i < ex.Errors.Count; i++)
                {
                    errorMsg.Append("Индекс: #" + i + "\n" +
                        "Ошибка: " + ex.Errors[i].Message + "\n" +
                        "Номер строки: " + ex.Errors[i].LineNumber + "\n" +
                        "Источник: " + ex.Errors[i].Source + "\n" +
                        "Процедура: " + ex.Errors[i].Procedure + "\n");
                }
                MessageBox.Show(errorMsg.ToString());
            }
            return connection;
        }
        public static void AddToTable(List<Person> data)
        {
            SqlBulkCopy bulkCopy = new SqlBulkCopy(ConnectSql(),
                SqlBulkCopyOptions.TableLock |
                SqlBulkCopyOptions.FireTriggers |
                SqlBulkCopyOptions.UseInternalTransaction, null);

            bulkCopy.DestinationTableName = tableName;
            bulkCopy.BatchSize = batchSize;
            string[] colMap = { "Id", "Date", "FirstName", "LastName", "SurName", "City", "Country" };

            using (ObjectReader reader = ObjectReader.Create(data, colMap))
            {
                bulkCopy.WriteToServer(reader);
            }
            ConnectSql().Close();
        }
        public static void ClearTable()
        {
            SqlCommand ClearDetail = new SqlCommand("TRUNCATE TABLE " + tableName + ";", ConnectSql());
            ClearDetail.ExecuteNonQuery();
            ConnectSql().Close();
        }
    }
}